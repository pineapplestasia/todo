package toDoList.pages;

import toDoList.repository.ToDoListRepository;
import toDoList.model.ToDoList;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.NativeButtonRenderer;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;

@Route("")
public class TaskList extends AppLayout {

    VerticalLayout layout;
    Grid<ToDoList> grid;
    RouterLink linkCreate;

    @Autowired
    ToDoListRepository toDoListRepository;

    public TaskList() {
        layout = new VerticalLayout();
        grid = new Grid<>();
        linkCreate = new RouterLink("Создать задачу", ManageTasks.class, 0);
        layout.add(linkCreate);
        layout.add(grid);
        addToNavbar(new H3("Список задач"));
        setContent(layout);
    }

    @PostConstruct
    public void fillGrid() {
        List<ToDoList> tasks = toDoListRepository.findAll();
        if (!tasks.isEmpty()) {


            grid.addColumn(ToDoList::getName).setHeader("Название");
            grid.addColumn(ToDoList::getDeadline).setHeader("Срок выполнения");

            grid.addColumn(new NativeButtonRenderer<>("Редактировать", toDoList -> {
                UI.getCurrent().navigate(ManageTasks.class, toDoList.getId());
            }));
            grid.addColumn(new NativeButtonRenderer<>("Удалить", toDoList -> {
                Dialog dialog = new Dialog();
                Button confirm = new Button("Удалить");
                Button cancel = new Button("Отмена");
                dialog.add("Вы уверены, что хотите удалить задачу?");
                dialog.add(confirm);
                dialog.add(cancel);

                confirm.addClickListener(clickEvent -> {
                    toDoListRepository.delete(toDoList);
                    dialog.close();
                    Notification notification = new Notification("Задача удалена", 1000);
                    notification.setPosition(Notification.Position.MIDDLE);
                    notification.open();

                    grid.setItems(toDoListRepository.findAll());

                });

                cancel.addClickListener(clickEvent -> {
                    dialog.close();
                });

                dialog.open();

            }));

            grid.setItems(tasks);

            grid.addItemClickListener(task -> {
                getUI().ifPresent(ui ->
                        ui.navigate(TaskContent.class, task.getItem().getId()));
            });
        }
    }


}
