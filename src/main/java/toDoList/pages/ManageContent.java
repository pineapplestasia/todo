package toDoList.pages;

import toDoList.repository.ToDoListRepository;
import toDoList.model.Task;
import toDoList.model.ToDoList;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route("manageContent")
public class ManageContent extends AppLayout implements HasUrlParameter<Integer> {

    Integer id;
    FormLayout taskForm;
    TextField name;
    Button saveTask;

    @Autowired
    ToDoListRepository toDoListRepository;

    public ManageContent() {

        taskForm = new FormLayout();
        name = new TextField("Название");
        saveTask = new Button("Сохранить");
        taskForm.add(name, saveTask);
        setContent(taskForm);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer taskId) {
        id = taskId;

        addToNavbar(new H3("Создание подзадачи"));
        fillForm();
    }


    public void fillForm() {

        saveTask.addClickListener(clickEvent -> {

            ToDoList toDoList = toDoListRepository.findById(id)
                    .orElseThrow(() -> new RuntimeException("Unable to find task list with passed id=" + id));
            Task task = new Task();
            task.setContent(name.getValue());
            toDoList.addTasks(task);
            toDoListRepository.save(toDoList);

            Notification notification = new Notification("Подзадача создана", 1000);
            notification.setPosition(Notification.Position.MIDDLE);
            notification.addDetachListener(detachEvent -> {
                UI.getCurrent().navigate(TaskContent.class, id);
            });
            taskForm.setEnabled(false);
            notification.open();
        });
    }
}
