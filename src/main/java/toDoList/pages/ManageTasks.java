package toDoList.pages;

import toDoList.repository.ToDoListRepository;
import toDoList.model.ToDoList;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.Optional;

@Route("manageTasks")
public class ManageTasks extends AppLayout implements HasUrlParameter<Integer> {

    Integer id;
    FormLayout taskForm;
    TextField name;
    TextField deadline;
    Button saveTask;

    @Autowired
    ToDoListRepository toDoListRepository;

    public ManageTasks() {

        taskForm = new FormLayout();
        name = new TextField("Название");
        deadline = new TextField("Срок выполнения");
        saveTask = new Button("Сохранить");

        taskForm.add(name, deadline, saveTask);
        setContent(taskForm);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer taskId) {
        id = taskId;
        if (!id.equals(0)) {
            addToNavbar(new H3("Редактирование задачи"));
        } else {
            addToNavbar(new H3("Создание задачи"));
        }
        fillForm();
    }


    public void fillForm() {

        if (!id.equals(0)) {
            Optional<ToDoList> tasks = toDoListRepository.findById(id);
            tasks.ifPresent(toDoList -> {
                name.setValue(toDoList.getName());
                deadline.setValue(toDoList.getDeadline());
            });
        }

        saveTask.addClickListener(clickEvent -> {

            ToDoList task = new ToDoList();
            if (!id.equals(0)) {
                task.setId(id);
            }
            task.setName(name.getValue());
            task.setDeadline(deadline.getValue());
            toDoListRepository.save(task);

            Notification notification = new Notification(id.equals(0) ? "Задача создана" : "Задача была изменена", 1000);
            notification.setPosition(Notification.Position.MIDDLE);
            notification.addDetachListener(detachEvent -> {
                UI.getCurrent().navigate(TaskList.class);
            });
            taskForm.setEnabled(false);
            notification.open();
        });
    }
}