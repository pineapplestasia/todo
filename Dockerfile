FROM openjdk:17-jdk-alpine
MAINTAINER pineapplestasia
COPY ./target/pet-1.0-SNAPSHOT.jar /app.jar
EXPOSE 8080:8080

ARG BUILD_DATE
ARG NAME
ARG URL
ARG VCS_REF

LABEL org.opencontainers.image.created=$BUILD_DATE
LABEL org.opencontainers.image.name=$NAME
LABEL org.opencontainers.image.url=$URL
LABEL org.opencontainers.image.revision=$VCS_REF

ENTRYPOINT exec java $JAVA_OPTS -jar /app.jar
