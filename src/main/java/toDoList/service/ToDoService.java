package toDoList.service;

import toDoList.repository.ToDoListRepository;
import toDoList.model.ToDoList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ToDoService {

    private ToDoListRepository toDoListRepository;

    public void save(ToDoList todo) {
        toDoListRepository.save(todo);
    }

    public List<ToDoList> listAll() {
        return (List<ToDoList>) toDoListRepository.findAll();
    }

    public ToDoList get(int id) {
        return toDoListRepository.findById(id).get();
    }

    public void delete(int id) {
        toDoListRepository.deleteById(id);
    }

    @Autowired
    public void setToDoListRepository(ToDoListRepository toDoListRepository) {
        this.toDoListRepository = toDoListRepository;
    }
}
