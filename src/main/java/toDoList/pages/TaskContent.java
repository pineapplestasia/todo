package toDoList.pages;

import toDoList.repository.TaskRepository;
import toDoList.repository.ToDoListRepository;
import toDoList.model.Task;
import toDoList.model.ToDoList;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.NativeButtonRenderer;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Route("taskContent")
public class TaskContent extends AppLayout implements HasUrlParameter<Integer> {

    Integer id;
    VerticalLayout layout;
    Grid<Task> grid;
    RouterLink linkCreate;
    Button back = new Button("Назад");

    @Autowired
    ToDoListRepository toDoListRepository;
    @Autowired
    TaskRepository taskRepository;

    public TaskContent() {
        layout = new VerticalLayout();
        grid = new Grid<>();
        back.addClickListener(task -> {
            UI.getCurrent().navigate(TaskList.class);
        });
        layout.add(grid, back);
        addToNavbar(new H3("  Список подзадач"));
        setContent(layout);
    }


    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer taskId) {
        id = taskId;
        fillForm();
    }

    public void fillForm() {


        linkCreate = new RouterLink("Создать подзадачу", ManageContent.class, id);
        layout.add(linkCreate);

        if (!id.equals(0)) {
            Optional<ToDoList> content = toDoListRepository.findById(id);

            content.ifPresent(toDoList -> {
                grid.addColumn(Task::getContent).setHeader("Название");


                grid.addColumn(new NativeButtonRenderer<>("Удалить", task -> {
                    Dialog dialog = new Dialog();
                    Button confirm = new Button("Удалить");
                    Button cancel = new Button("Отмена");
                    dialog.add("Вы уверены что хотите удалить подзадачу?");
                    dialog.add(confirm);
                    dialog.add(cancel);


                    confirm.addClickListener(clickEvent -> {

                        removeTaskAndRefresh(task);
                        dialog.close();
                        Notification notification = new Notification("Подзадача удалена", 1000);
                        notification.setPosition(Notification.Position.MIDDLE);
                        notification.open();


                    });

                    cancel.addClickListener(clickEvent -> {
                        dialog.close();
                    });

                    dialog.open();

                }));

                grid.setItems(toDoList.getTasks());

            });
        }
    }

    private void removeTaskAndRefresh(Task task) {
        ToDoList toDoList = toDoListRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Unable to find toDoList with passed id=" + id));

        toDoList.getTasks().remove(task);
        toDoList = toDoListRepository.save(toDoList);
        grid.setItems(toDoList.getTasks());
    }
}

